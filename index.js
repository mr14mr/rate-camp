var express = require('express');
var app = express();
var moment = require('moment');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var path = require('path');


app.use(express.static(path.join(__dirname, 'public')));

var url = 'mongodb://localhost:27017/test';
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server.");
  db.close();
});

var findData = function(db, callback) {
   var cursor =db.collection('data').find( );
   var returnString = [];
   cursor.each(function(err, doc) {
      assert.equal(err, null);
      if (doc != null){
      	if (doc.time == moment().format("D.M.YYYY")) {
			//console.log(doc.time);
			returnString.push(doc);
	      } 
      } else {
        callback(returnString);
      }
   });
};

app.get('/api/getvalue', function(req, res){
	var data;
	MongoClient.connect(url, function(err, db) {
		assert.equal(null, err);
		findData(db, function(string) {
			var result = 0;
			data = string;
			db.close();
			//console.log(data);
			for (var i = data.length - 1; i >= 0; i--) {
				//console.log(data[i]['mark']);
				result+=parseInt(data[i]['mark']);
			};
			result/=data.length;
			res.json({value: result});
		});	
	});

});

var insertData = function(db, mark, callback) {
   db.collection('data').insertOne( {
      "time" : moment().format("D.M.YYYY"),
      "mark" : mark
   }, function(err, result) {
    assert.equal(err, null);
    //console.log("Inserted a document into the data collection.");
    callback(result);
  });
};


app.get('/api/setdata', function(req, res){
	if(!req.query.mark || req.query.mark < 0 || req.query.mark > 5){
		res.sendStatus(500);
		return false;
	}

	MongoClient.connect(url, function(err, db) {
	  assert.equal(null, err);
	  insertData(db,req.query.mark, function() {
	    	db.close();
	  });
	});

	/*
	console.log(req.query.login);
	console.log(req.query.pass);
	*/
	res.sendStatus(200);
});

app.get('/', function(req, res){
	res.sendFile(__dirname + '/pool.html');
});
app.get('/dashboard', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

app.listen(80);